class Transformer {
    constructor(name, health) {
        this.name=name;
        this.health=health;
    }
    attack(){}

    hit(weapon){
        this.health=this.health-weapon.damage;
    }

    isDead(){
        return this.health <= 0;
    }
}
 /*Создать класс Autobot, который наследуется от класса Transformer.
Имеет свойсто weapon (экземпляр класса Weapon), т.к. автоботы сражаются с использованием оружия.
Конструктор класса принимает 2 параметра: имя трансформера и оружее (экземпляр класса Weapon).
Метод attack возвращает результат использования оружия weapon.fight()
  */
class  Autobot extends Transformer {
    constructor(name, weapon){
        super(name,  100);
        this.weapon=weapon;
    }

    attack(){
        return this.weapon.fight();
    }
}

/*Создать класс Deceptikon который наследуется от класса Transformer.
Десептиконы не пользуются оружием, поэтому у них нет свойства weapon. За то они могут иметь разное количество здоровья.
Конструктор класса принимает 2 параметра: имя name и количество здоровья health
Метод attack возвращает характеристики стандартного вооружения, например,
{ damage: 5, speed: 1000 }, где damage - это кличество урона, а speed - скорость атаки в мс.
 */

class Deceptikon extends Transformer {
    attack(){
        return {
            damage: 5,
            speed: 1000,
        };
    }
}

/*Создать класс оружия Weapon, на вход принимает 2 параметра: damage - урон и speed - скорость атаки.
Имеет 1 метод fight, который возвращает характеристики оружия в виде { damage: 5, speed: 1000 }
 */

class Weapon {
    constructor (damage, speed){
        this.damage=damage;
        this.speed=speed;
    }

    fight(){
       return {
           damage: this.damage,
           speed: this.speed,
       };
    }
}

/*
Создать 1 автобота с именем OptimusPrime с оружием, имеющим характеристики { damage: 100, speed: 1000 }
 */

// const weapon1=new Weapon(100, 1000);
//
// const optimusPrime =new Autobot('OptimusPrime', weapon1);

/*
-Создать 1 десептикона с именем Megatron и показателем здоровья 10000
-Посмотреть что происходит при вызове метода atack() у траснформеров разного типа, посмотреть сигнатуры классов
-Вопрос: сколько нужно автоботов чтобы победить Мегатрона
если параметр speed в оружии это количество милсекунд до следующего удара? Реализовать симуляцию боя.
 */

// const megatron= new Deceptikon('Megatron', 1000);
//
// console.log(optimusPrime.attack());
// console.log(megatron.attack());
//
// console.log(megatron.health);
// megatron.hit(optimusPrime.attack());
// console.log(megatron.health);

class Arena {
    constructor(side1, side2){
        this.side1=side1;  // массив Transformer
        this.side2=side2; // массив Transformer
    }

    fight(){
       for (let i=0; i<this.side1.length; i++ ){
           let transformer=this.side1[i];
           setTimeout(this._hitSomebody, transformer.attack().speed, transformer,this.side2, this);
       }
        for (let i=0; i<this.side2.length; i++ ){
            let transformer=this.side2[i];
            setTimeout(this._hitSomebody, transformer.attack().speed, transformer,this.side1, this);
        }
    }

    _hitSomebody(transformer, side, arena){
        if(transformer.isDead()){
            return;
        }
        if (side.length===0){
            return;
        }

        console.log(transformer.name+' атаковал ' + side[0].name);
        side[0].hit(transformer.attack());
        if (side[0].isDead()){
            console.log(transformer.name+' убил ' + side[0].name);
            side.shift();
        }
        // render
        setTimeout(arena._hitSomebody, transformer.attack().speed, transformer, side, arena);
    }
}

const side1= [
    new Autobot('Pikachu', new Weapon(10,400)),
    new Autobot('Harry Potter', new Weapon(20,500)),
    new Autobot('Dobby', new Weapon(1,100)),
];

const side2= [
    new Deceptikon('Lord Voldemort', 1000),
    new Deceptikon('Basilisk', 100),
];

// const arena=new Arena(side1, side2);
// arena.fight();

class Visualizer{
    constructor(side1, side2){
        this.side1=side1;
        this.side2=side2;
        this.arena=new Arena(this.side1, this.side2);
        this.elArenaSide1=document.querySelector('.arena-side-1');
        this.elArenaSide2=document.querySelector('.arena-side-2');
    }
    render(){
        this._clearSide( this.elArenaSide1);
        this._clearSide(this.elArenaSide2);
       this._renderSide(this.side1,  this.elArenaSide1);
        this._renderSide(this.side2,  this.elArenaSide2);
    }

    _clearSide(elArenaSide){
        elArenaSide.innerHTML="";
    }

    _renderSide(side, elArenaSide){
        for (let i=0; i<side.length; i++){
            const elDiv=document.createElement('div');
            elDiv.classList.add('bot');
            const elSpan=document.createElement('span');
            elSpan.innerText=side[i].health + ' hp';
            elDiv.appendChild(elSpan);
            elArenaSide.appendChild(elDiv);
        }
    }

    fight(){
        this.arena.fight();
        setInterval(()=>{this.render()}, 200);
    }
}

const v=new Visualizer(side1, side2);
v.render();
v.fight();